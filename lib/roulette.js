function RouletteService(randomJs, credits, logger) {
    var send = function (msg) {
        console.log("send not implemented")
    };
    var roulette = false;
    var rouletteBets = [];
    const RED = 'red';
    const BLACK = 'black';
    const GREEN = 'green';

    // Europe roulette wheel numbers
    const redNumbers = [1, 3, 5, 7, 9, 12, 14, 16, 18, 19, 21, 23, 25, 27, 30, 32, 34, 36];
    const blackNumbers = [2, 4, 6, 8, 10, 11, 13, 15, 17, 20, 22, 24, 26, 28, 29, 31, 33, 35];

    function rouletteColor(number) {
        if (redNumbers.includes(number)) {
            return RED
        } else if (blackNumbers.includes(number)) {
            return BLACK
        }

        return GREEN;
    }

    function rouletteColorImage(color) {
        if (color === RED) {
            return ':red_circle:';
        } else if (color === BLACK) {
            return ':black_circle:';
        } else if (color === GREEN) {
            return ':green_circle:';
        }

        return 'unknown';
    }

    function rouletteParseEmojji(emojiNumber) {
        logger.info(`Emoji for roulette : ${emojiNumber}`);
        if (emojiNumber === 128308) {
            return RED;
        } else if (emojiNumber === 9899) {
            return BLACK;
        } else if (emojiNumber === 128994) {
            return GREEN;
        }
    }

    function startRoulette(player) {
        if (! isActive()) {
            roulette = true;
            rouletteBets = [];
            send = player.sendMessage;
            send('Roulette is begonnen! (!bet 100 :red_circle:, :black_circle: of :green_circle:)');
            logger.info('Starting roulette..');

            const rouletteCountDown = setTimeout(function() {
                roulette = false;

                if (rouletteBets.length === 0) {
                    send('Niemand heeft ingezet :FeelsBadMan:');
                    return
                }

                const rouletteNumber = randomJs.randomRouletteNumber();
                const color = rouletteColor(rouletteNumber);

                const winners = rouletteBets.filter(function (bet) {
                    return bet.color === color
                });
                const winnerNames = winners.map(function (bet) {
                    if (bet.color === GREEN) {
                        return `${bet.player.name} met ${bet.amount * 36}`
                    }
                    return `${bet.player.name} met ${bet.amount * 2}`
                }).join(',');

                const losers = rouletteBets.filter(function (bet) {
                    return bet.color !== color
                });
                const loserNames = losers.map(function (bet) {
                    return `${bet.player.name} met ${bet.amount}`
                }).join(',');

                winners.forEach(function (winner) {
                    if (winner.color === GREEN) {
                        credits.addCredits(winner.player.creditsId, Number(winner.amount * 36));
                    } else {
                        credits.addCredits(winner.player.creditsId, Number(winner.amount * 2));
                    }
                });

                const colorImage = rouletteColorImage(color);
                const resultMessage = `\nRien ne va plus... :game_die: Hij is gevallen op nummer ${rouletteNumber} dat betekent kleur ${colorImage} !\n`;

                if (losers.length === 0) {
                    send(resultMessage + `Iedereen heeft gewonnen :partying_face: \nGefeliciteerd ${winnerNames}\n`);
                } else if (winners.length === 0) {
                    send(resultMessage + `Iedereen is alles kwijt :coffin:\n`);
                } else {
                    send(resultMessage + `Gefeliciteerd ${winnerNames}\nHelaas ${loserNames}\n`);
                }
            }, 30000);
        } else {
            send(`We kunnen maar 1 roulette tegelijk spelen`);
        }
    }

    function isActive() {
        return roulette;
    }

    function setRouletteBet(player, amount, emoji) {
        const color = rouletteParseEmojji(emoji);

        if (color === RED || color === BLACK || color === GREEN) {
            if (rouletteBets.filter(function (element) {return element.player.id === player.id}).length > 0) {
                logger.info(`${player.name} already placed a bet`);
                player.sendMessage(`${player.name} je hebt al ingezet.`);
                return
            }

            credits.removeCredits(player.creditsId, amount);
            rouletteBets.push({player: player, color: color, amount: amount});
            logger.info(`${player.name} placed ${amount} on ${color}`);
            player.sendMessage(`${player.name} je ${amount} inzet op ${rouletteColorImage(color)} is ontvangen.`);
        }
    }

    return {
        startRoulette,
        setRouletteBet,
        isActive
    }
}

module.exports = RouletteService;