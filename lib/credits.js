

function CreditsService(redis, logger) {

    function setCredits(id, amount) {
        logger.info(`Setting ${amount} credits for ${id}`);
        redis.set(id, Number(amount));
    }

    function addCredits(id, amount) {
        logger.info(`Adding ${amount} credits to ${id}`);
        getCredits(id).then(function (credits) {
            redis.set(id, (credits + Number(amount)));
        });
    }

    function removeCredits(id, amount) {
        logger.info(`Removing ${amount} credits from ${id}`);
        getCredits(id).then(function (credits) {
            logger.info(`Balance is ${credits} from ${id}`);
            if (amount > credits) {
                logger.info(`Cannot substract ${amount} credits from ${id}, balance is only ${credits}`);
                return
            }

            logger.info(`New balance ${(credits - Number(amount))} from ${id}`);

            redis.set(id, (credits - Number(amount)));
        });
    }

    async function getCredits(id) {
        return await redis.getAsync(id).then(function (value) {
            if (value === null) {
                return 0;
            } else {
                return Number(value);
            }
        });
    }

    return {
        setCredits,
        addCredits,
        removeCredits,
        getCredits
    };
}

module.exports = CreditsService;