function parse(tip) {
    const tipNumber = parseInt(tip);

    if (isNaN(tipNumber)) {
        return 'unknown';
    }

    var message = '';
    switch (tipNumber) {
        case 6 : message = `Speltip 6 – Spreid je winstkansen`; break;
        case 7 : message = `Speltip 7 – Vertrouw niet alleen op geluksgetallen`; break;
        case 9 : message = `Speltip 9 – Weet wanneer je moet stoppen`; break;
        case 10 : message =  `Speltip 10 – Denk goed na voor je inzet`; break;
        case 11 : message =  `Speltip 11 – Werk aan je pokerface`; break;
        case 12 : message =  `Speltip 12 – Bluf alleen als het geloofwaardig is`; break;
        case 14 : message =  `Speltip 14 – Weet waar je kansen liggen`; break;
        case 15 : message =  `Speltip 15 – Vergeet de spelregels niet`; break;
        case 16 : message =  `Speltip 16 – Laat je niet in je kaarten kijken`; break;
        case 17 : message =  `Speltip 17 – Meedoen maakt het spannender`; break;
        case 18 : message =  `Speltip 18 – Niet alles is bluf`; break;
        default: message = 'unknown'; break;
    }

    return message;
}

exports.parseTip = function (tip) {
    return parse(tip);
};
