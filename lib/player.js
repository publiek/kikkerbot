
function PlayerFactory(config, logger) {

    function hasPermission(member) {
        return member.roles.cache.some(r => config.authorized_roles.includes(r.name));
    }

    function creditsId(guild, user) {
        return `${guild.id}-${user.id}-credits`;
    }

    function fromMessage(message) {
        if (! hasPermission(message.member)) {
            logger.warn("Cannot create player, player has not the right permissions.");
            return
        }

        return createPlayer(message.member, message.channel);
    }

    function fromMention(message, mention) {
        const matches = mention.match(/^<@!?(\d+)>$/);
        if (!matches) return;

        const member = message.channel.members.cache.get(matches[1]);

        if ( ! member) {
            logger.warn(`Could not found member ${matches[1]} in guild`);
            return
        }

        if (! hasPermission(member)) {
            logger.warn("Cannot create player, player has not the right permissions.");
            return
        }

        return createPlayer(member, message.channel);
    }

    function allFromChannel(channel) {
        return channel.guild.members.cache.filter(function(member) {
            return hasPermission(member)
        }).map(function(member) {
            return createPlayer(member, channel)
        });
    }

    function createPlayer(member, channel) {
        return {
            id: member.user.id,
            name: member.user.username,
            creditsId: creditsId(member.guild, member.user),
            isOnline: (member.presence?.status !== 'offline'),
            isOwner: (member.user.id === config.owner_id),
            sendMessage: function (msg) {
                if (msg && !msg.isEmpty && msg !== '\n') {
                    channel.send(msg);
                }
            }
        }
    }

    return {
        fromMessage,
        fromMention,
        allFromChannel
    }
}

module.exports = PlayerFactory;