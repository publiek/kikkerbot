exports.currentEpochDay = function () {
    const minutes = 1000 * 60;
    const hours = minutes * 60;
    const days = hours * 24;

    const d = new Date();
    const t = d.getTime();

    return Math.round(t / days);
};

exports.parseCredit = function (value) {
    return (isNaN(value) ? 0 : Math.round(Math.abs(Number(value))));
};

const RandomJs = require('random-js');
// create a Mersenne Twister-19937 that is auto-seeded based on time and other random values
const engine = RandomJs.MersenneTwister19937.autoSeed();

// create a distribution that will consistently produce integers within inclusive range [0, 99].
const rouletteDestribution = RandomJs.integer(0, 36);
exports.randomRouletteNumber = function() {
    return rouletteDestribution(engine);
};

exports.randomBoolean = function() {
    return RandomJs.bool()(engine);
};

const giftDestribution = RandomJs.integer(1, 15);
exports.randomGiftAmount = function () {
    return (giftDestribution(engine) * 1000);
};

const betDestribution = RandomJs.integer(0, 2);
exports.randomBet = function () {
    return betDestribution(engine);
};

const delayDestribution = RandomJs.integer(5000, 15000);
exports.randomMillisecondsDelay = function () {
    return delayDestribution(engine);
};

const spelTipDistribution = [6,7,9,10,11,12,14,15,16,17,18];
exports.randomSpelTipNumber = function () {
    return RandomJs.pick(engine, spelTipDistribution);
};

exports.shuffleArray = function (array) {
  return RandomJs.shuffle(engine, array)
};