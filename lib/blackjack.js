function createCard(name, icon, value, suit) {
    return {
        name: name,
        icon: icon,
        value: value,
        suit: suit
    }
}

function calculateHiLoCardScore(card) {
    const low = [2, 3, 4, 5, 6];
    const hi = [10, 11];

    if (low.includes(card.value)) {
        return 1;
    } else if (hi.includes(card.value)) {
        return -1;
    } else {
        return 0;
    }
}

function Shoe(randomJs, logger) {
    function assembleDeck() {
        return [
            createCard('two', '2 of ♣', 2, 'clovers'), createCard('three', '3 of ♣', 3, 'clovers'), createCard('four', '4 of ♣', 4, 'clovers'), createCard('five', '5 of ♣', 5, 'clovers'), createCard('six', '6 of ♣', 6, 'clovers'), createCard('seven', '7 of ♣', 7, 'clovers'), createCard('eight', '8 of ♣', 8, 'clovers'), createCard('nine', '9 of ♣', 9, 'clovers'), createCard('ten', '10 of ♣', 10, 'clovers'), createCard('jack', 'J of ♣', 10, 'clovers'), createCard('queen', 'Q of ♣', 10, 'clovers'), createCard('king', 'K of ♣', 10, 'clovers'), createCard('ace', 'A of ♣', 11, 'clovers'),
            createCard('two', '2 of ♥', 2, 'hearts'), createCard('three', '3 of ♥', 3, 'hearts'), createCard('four', '4 of ♥', 4, 'hearts'), createCard('five', '5 of ♥', 5, 'hearts'), createCard('six', '6 of ♥', 6, 'hearts'), createCard('seven', '7 of ♥', 7, 'hearts'), createCard('eight', '8 of ♥', 8, 'hearts'), createCard('nine', '9 of ♥', 9, 'hearts'), createCard('ten', '10 of ♥', 10, 'hearts'), createCard('jack', 'J of ♥', 10, 'hearts'), createCard('queen', 'Q of ♥', 10, 'hearts'), createCard('king', 'K of ♥', 10, 'hearts'), createCard('ace', 'A of ♥', 11, 'hearts'),
            createCard('two', '2 of ♦', 2, 'diamonds'), createCard('three', '3 of ♦', 3, 'diamonds'), createCard('four', '4 of ♦', 4, 'diamonds'), createCard('five', '5 of ♦', 5, 'diamonds'), createCard('six', '6 of ♦', 6, 'diamonds'), createCard('seven', '7 of ♦', 7, 'diamonds'), createCard('eight', '8 of ♦', 8, 'diamonds'), createCard('nine', '9 of ♦', 9, 'diamonds'), createCard('ten', '10 of ♦', 10, 'diamonds'), createCard('jack', 'J of ♦', 10, 'diamonds'), createCard('queen', 'Q of ♦', 10, 'diamonds'), createCard('king', 'K of ♦', 10, 'diamonds'), createCard('ace', 'A of ♦', 11, 'diamonds'),
            createCard('two', '2 of ♠', 2, 'spades'), createCard('three', '3 of ♠', 3, 'spades'), createCard('four', '4 of ♠', 4, 'spades'), createCard('five', '5 of ♠', 5, 'spades'), createCard('six', '6 of ♠', 6, 'spades'), createCard('seven', '7 of ♠', 7, 'spades'), createCard('eight', '8 of ♠', 8, 'spades'), createCard('nine', '9 of ♠', 9, 'spades'), createCard('ten', '10 of ♠', 10, 'spades'), createCard('jack', 'J of ♠', 10, 'spades'), createCard('queen', 'Q of ♠', 10, 'spades'), createCard('king', 'K of ♠', 10, 'spades'), createCard('ace', 'A of ♠', 11, 'spades'),
        ];
    }

    var hiLoCardScore = 0;
    var totalCards = 1;
    var deckCount = 2;
    var stack = createStack(deckCount);

    function createStack(decks) {
        var deck = [];
        for (let i = 0; i < decks; i++) {
            deck = deck.concat(assembleDeck());
        }

        logger.info(`created a stack from ${decks} decks in total ${deck.length} cards...`);

        hiLoCardScore = 0;
        totalCards = deck.length;
        return randomJs.shuffleArray([...deck]);
    }

    function draw(player = null) {
        if (shouldShuffle()) {
            if (player) {
                player.sendMessage('Shuffling and creating a new shoe, reset your card counting ;)');
            }
            reset(deckCount);
        }

        const drawnCard = stack.pop();
        hiLoCardScore += calculateHiLoCardScore(drawnCard);

        return drawnCard;
    }

    function reset(givenCount) {
        deckCount = givenCount;
        stack = createStack(deckCount);
    }

    function shouldShuffle() {
        const percentage = (stack.length / totalCards) * 100;
        logger.info(`Current stack size percentage ${percentage} %`);
        return (percentage < 25) // 3/4 of the stack has been used
    }

    function getDecksRemaining() {
        return Math.round((stack.length / 52) * 10) / 10;
    }

    function getCardCount() {
        return hiLoCardScore;
    }

    return {
        draw,
        reset,
        getDecksRemaining,
        getCardCount
    }
}

function PlayerHands(logger) {
    var playerHands = [];

    function addBet(player, amount) {
        playerHands.push({player: player, amount: amount, dead: false, stand: false, doubleDown: false, hand: []});
    }

    function addCard(player, card) {
        playerHands = playerHands.map(function (element) {
            if (element.player.id === player.id) {
                logger.info(`Added card ${card.name} ${card.suit} to ${element.player.name}`);
                element.hand.push(card);
            }

            return element;
        });
    }

    function getAll() {
        return playerHands;
    }

    function getHand(player) {
        const result = playerHands.find(element => element.player.id === player.id);
        if (result) {
            return result.hand
        }
    }

    function getBet(player) {
        return playerHands.find(function (element) {
            return element.player.id === player.id
        });
    }

    function setStand(player) {
        playerHands = playerHands.map(function (element) {
            if (element.player.id === player.id) {
                element.stand = true;
            }

            return element;
        });
    }

    function setDead(player) {
        playerHands = playerHands.map(function (element) {
            if (element.player.id === player.id) {
                element.dead = true;
            }

            return element;
        });
    }

    function setDoubleDown(player) {
        playerHands = playerHands.map(function (element) {
            if (element.player.id === player.id) {
                element.doubleDown = true;
            }

            return element;
        });
    }

    function isComplete(player) {
        const result = playerHands.find(function (element) {
            return element.player.id === player.id
        });

        return (result.dead || result.stand);
    }

    function allComplete() {
        var complete = true;
        playerHands.forEach(function(element) {
            if (!element.dead && ! element.stand) {
                complete = false;
            }
        });

        return complete;
    }

    function forceStandForNotComplete(handlePlayerCallback = null) {
        playerHands = playerHands.map(function (element) {
            if ( ! element.dead && !element.stand) {
                element.stand = true;

                if (handlePlayerCallback) {
                    handlePlayerCallback(element.player);
                }
            }
            return element;
        });
    }

    function reset() {
        playerHands = [];
    }

    return {
        getBet,
        addBet,
        getHand,
        addCard,
        setDead,
        setStand,
        setDoubleDown,
        forceStandForNotComplete,
        getAll,
        isComplete,
        allComplete,
        reset
    }
}

function BlackJackService(randomJs, credits, logger) {
    //blackjack 1.5x inzet
    //dealer blijft op 17
    //75% kaarten gebruikt wordt er opnieuw geshuffeld
    //Player busts als eerste dan is hij zijn inzet meteen kwijt

    //!hit
    //!stand
    //!double down

    //Todo: !split

    var started = false;
    var drawing = false;
    const playerHands = new PlayerHands(logger);
    var dealer = [];
    var shoe = new Shoe(randomJs, logger);
    var forceOnInactivityTimeout = null;
    var continueBetters = [];

    function setDeckCount(player, deckCount) {
        if (deckCount > 0 && deckCount < 100) {
            shoe.reset(deckCount);
            player.sendMessage(`Created a new shoe with ${deckCount} decks`);
        } else {
            player.sendMessage('Cannot update amount of decks in the shoe, must be between 0 and 100');
        }
    }

    function start(player, withContinue = false) {
        if (started) {
            logger.info('Cannot start blackjack, already playing');

            return
        }

        playerHands.reset();
        dealer = [];
        started = true;
        drawing = false;

        continueBetters.forEach(function (continueBetter) {
            credits.getCredits(continueBetter.player.creditsId).then(continueCredits => {
                if (continueBetter.amount > continueCredits) {
                    logger.info(`Removing player ${continueBetter.player.name} from blackjack continue better list`);
                    continueBetter.player.sendMessage(`${continueBetter.player.name} je hebt niet meer genoeg credits (${continueCredits}) om mee te doen met blackjack`);
                    removeBetContinue(continueBetter.player);
                } else {
                    logger.info(`Player ${continueBetter.player.name} goes for another blackjack round`);
                    credits.removeCredits(continueBetter.player.creditsId, continueBetter.amount);
                    playerHands.addBet(continueBetter.player, continueBetter.amount)
                }
            });
        });

        if (withContinue) {
            const continueNames = continueBetters.map(function (better) {
               return better.player.name;
            });
            player.sendMessage(`New round with continue players (${continueNames})`);
        } else {
            player.sendMessage(`Blackjack gaat starten place your bets!`);
        }
        setTimeout(function () {
           drawInitialRound(player);
        }, 15000);
    }

    function drawInitialRound(player) {
        drawing = true;
        dealer.push(shoe.draw(player), shoe.draw(player));

        let stateMessage = `Dealer has _one hidden card_ and a ${dealer[0].icon}\n`;
        logger.info(`Added card ${dealer[0].name} ${dealer[0].suit} and ${dealer[1].name} ${dealer[1].suit} to dealer`);

        playerHands.getAll().forEach(function(playerHand) {
            const first = shoe.draw(player);
            const second = shoe.draw(player);
            stateMessage += `${playerHand.player.name} has a ${first.icon} and a ${second.icon} (${getScore([first, second])})\n`;

            playerHands.addCard(playerHand.player, first);
            playerHands.addCard(playerHand.player, second);
        });

        player.sendMessage(stateMessage);
        forceOnInactivityTimeout = forceHandOnInactivity(player);
    }

    function forceHandOnInactivity(player) {
        return setTimeout(function ()  {
            logger.info('Took to long, forcing inactive players into a stand');
            playerHands.forceStandForNotComplete(removeBetContinue);
            checkRoundComplete(player);
        }, 60000); // 1 minute
    }

    function setBet(player, amount) {
        if (playerHands.getBet(player)) {
            logger.info(`${player.name} already placed a bet for blackjack`);
            player.sendMessage(`${player.name} je hebt al ingezet.`);
            return
        }

        credits.removeCredits(player.creditsId, amount);
        playerHands.addBet(player, amount);
        logger.info(`${player.name} placed ${amount} a bet for blackjack`);
        player.sendMessage(`${player.name} je ${amount} inzet voor blackjack is ontvangen.`);
    }

    function setBetContinue(player, amount) {
        logger.info(`Added ${player.name} to continue bet list with bet ${amount}.`);

        const exists = continueBetters.find(function (element) {
           return player.id === element.player.id
        });

        if ( ! exists) {
            continueBetters.push({player: player, amount: amount});
            setBet(player, amount);
        } else {
            continueBetters = continueBetters.map(function (element) {
                if (player.id === element.player.id) {
                    element.amount = amount;
                    logger.info(`Updated ${player.name} continue bet amount.`);
                    player.sendMessage(`${player.name} je inzet is geupdate.`);
                }

                return element
            });
        }
    }
    
    function removeBetContinue(player) {
        logger.info(`Removed ${player.name} from continue bet list.`);
        player.sendMessage(`${player.name} is gestopt blackjack.`);

        continueBetters = continueBetters.filter(function (element) {
            return player.id !== element.player.id
        });
    }

    function doubleDown(player) {
        if ( ! playerHands.getBet(player) || playerHands.isComplete(player)) {
            logger.info(`Cannot hit for ${player.name} already on stand or is dead`);
            return
        }

        if ( ! isDealerReady()) {
            logger.info('Cannot hit dealer not ready');
            return;
        }

        const playerBet = playerHands.getBet(player);

        if (playerBet.hand.length !== 2) {
            logger.info('Can only double down after initial card draw');
            return;
        }

        credits.getCredits(playerBet.player.creditsId).then(playerCredits => {
            if (playerBet.amount > playerCredits) {
                logger.info(`${player.name} has not enough credits for doubledown`);
                player.sendMessage(`${player.name} je hebt niet genoeg credits  (${playerCredits}) voor een doubledown`);
                return;
            }

            credits.setCredits(playerBet.player.creditsId, (playerCredits - playerBet.amount));

            playerHands.setDoubleDown(player);
            hit(player, true);
        });
    }

    function hit(player, hasDoubleDown = false) {
        if ( ! playerHands.getBet(player) || playerHands.isComplete(player)) {
            logger.info(`Cannot hit for ${player.name} already on stand or is dead`);
            return
        }

        if ( ! isDealerReady()) {
            logger.info('Cannot hit dealer not ready');
            return;
        }

        var response = '';
        const card = shoe.draw(player);

        playerHands.addCard(player, card);
        const hand = playerHands.getHand(player);
        response += `${player.name} draws a ${card.icon}`;

        if (isDead(hand)) {
            playerHands.setDead(player);
            response += ` and is dead :skull_crossbones: (${getScore(hand)})\n`
        } else {
            response += `, total (${getScore(hand)})`
        }

        if (hasDoubleDown) {
            playerHands.setStand(player);
            response += ' and stands.'
        }
        response += '\n';

        player.sendMessage(response);
        checkRoundComplete(player);
    }

    function isDead(hand) {
        return getScore(hand) > 21;
    }

    function getScore(hand) {
        var total = 0;

        hand.forEach(function (card) {
            total += card.value
        });

        if (total > 21) {
            hand.sort((a,b) => a.value - b.value); //from low to high

            for (let card of hand) {
                // Aces can be 1 or 11
                if (card.name === 'ace') {
                    total -= 10;
                    if (total <= 21) {
                        break;
                    }
                }
            }
        }

        return total;
    }

    function hasBlackJack(hand) {
        return (getScore(hand) === 21 && hand.length === 2)
    }

    function stand(player) {
        if ( ! playerHands.getBet(player) || playerHands.isComplete(player)) {
            logger.info(`Cannot stand for ${player.name} already on stand or is dead`);
            return
        }

        if ( ! isDealerReady()) {
            logger.info('Cannot hit dealer not ready');
            return;
        }

        playerHands.setStand(player);
        checkRoundComplete(player);
    }

    function checkRoundComplete(player) {
        if (playerHands.allComplete()) {
            clearTimeout(forceOnInactivityTimeout);
            logger.info(`Round complete drawing for dealer`);

            if (hasBlackJack(dealer)) {
                player.sendMessage(`Dealer has ${dealer[0].icon} and a ${dealer[1].icon} (${getScore(dealer)}) BLACKJACK!`);
            } else {
                player.sendMessage(`Dealer has ${dealer[0].icon} and a ${dealer[1].icon} (${getScore(dealer)})`);
            }

            drawDealer(player);
        }
    }

    function payoutWin(playerHand, message) {
        var winAmount = 0;
        if (hasBlackJack(playerHand.hand)) {
            winAmount = Number(playerHand.amount) + Math.round(Number(playerHand.amount) * 1.5);
            message += `${playerHand.player.name} has a BLACKJACK and won ${winAmount}\n`;
        } else {
            if (playerHand.doubleDown) {
                winAmount = Number(playerHand.amount) * 4;
                message += `${playerHand.player.name} (${getScore(playerHand.hand)}) won with DOUBLEDOWN :fire: from the dealer and received ${winAmount}\n`;
            } else {
                winAmount =  Number(playerHand.amount) * 2;
                message += `${playerHand.player.name} (${getScore(playerHand.hand)}) won from the dealer and received ${winAmount}\n`;
            }
        }

        credits.addCredits(playerHand.player.creditsId, winAmount);

        return message;
    }

    function payoutPush(playerHand) {
        if (playerHand.doubleDown) {
            credits.addCredits(playerHand.player.creditsId, Number(playerHand.amount) * 2);
        } else {
            credits.addCredits(playerHand.player.creditsId, playerHand.amount);
        }
    }

    function completePayout(player) {
        var payoutMessage = '';
        playerHands.getAll().forEach(function (playerHand) {
            if ( ! playerHand.dead) {
                if (isDead(dealer)) {
                    payoutMessage = payoutWin(playerHand, payoutMessage);
                } else {
                    if (getScore(playerHand.hand) < getScore(dealer)) {
                        payoutMessage += `${playerHand.player.name} (${getScore(playerHand.hand)}) lost from the dealer.\n`
                    } else if (hasBlackJack(playerHand.hand) && !hasBlackJack(dealer)) {
                        payoutMessage = payoutWin(playerHand, payoutMessage);
                    } else if (hasBlackJack(playerHand.hand) && hasBlackJack(dealer)) {
                        payoutMessage += `${playerHand.player.name} and the dealer have both blackjack nobody wins.\n`;
                        payoutPush(playerHand);
                    } else if (getScore(playerHand.hand) === getScore(dealer)) {
                        payoutMessage += `${playerHand.player.name} and the dealer have the same score nobody wins.\n`;
                        payoutPush(playerHand);
                    } else if (getScore(playerHand.hand) > getScore(dealer)) {
                        payoutMessage = payoutWin(playerHand, payoutMessage);
                    }
                }
            }
        });

        if (payoutMessage === '') {
            payoutMessage += '\n'
        }

        logger.info('Payout complete');
        started = false;

        if (continueBetters.length > 0) {
            logger.info('We have continue betters start another round');
            player.sendMessage(payoutMessage);

            //Wait 5 seconds before we start the game
            setTimeout(function () {
                start(continueBetters[0].player, true)
            }, 500);
        } else {
            player.sendMessage(payoutMessage + "Blackjack stopped, thanks for playing!");
        }
    }

    function isDealerReady() {
        return dealer.length > 0;
    }

    function drawDealer(player) {
        if (isDead(dealer)) {
            player.sendMessage(`Dealer is dead :skull_crossbones:\n`);
            completePayout(player);
            return
        } else if (getScore(dealer) >= 17) {
            player.sendMessage(`Dealer stands on ${getScore(dealer)}\n`);
            completePayout(player);
            return
        }

        const card = shoe.draw(player);
        dealer.push(card);
        logger.info(`Dealer draws a new card ${card.name} ${card.suit}`);
        player.sendMessage(`Dealer draws a new card ${card.icon}, total (${getScore(dealer)})`);

        // Keep drawing
        drawDealer(player);
    }

    function isActive() {
        return started;
    }

    function isDrawing() {
        return drawing;
    }

    function getDecksRemaining() {
        return shoe.getDecksRemaining();
    }

    function getCardCount() {
        return shoe.getCardCount();
    }

    return {
        start,
        isActive,
        isDrawing,
        setBet,
        setBetContinue,
        setDeckCount,
        removeBetContinue,
        getCardCount,
        getDecksRemaining,
        hit,
        doubleDown,
        stand
    }
}

module.exports = BlackJackService;