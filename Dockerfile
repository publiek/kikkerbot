FROM node:16

WORKDIR /home/node/app

ARG REDIS_URL=redis://redis
ENV REDIS_URL=$REDIS_URL

COPY package*.json ./

RUN npm install
#RUN npm ci --only=production

COPY . .

EXPOSE 8080
CMD [ "node", "server.js" ]