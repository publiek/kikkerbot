/**
 * A kikkerbot
 */

const { Client, Intents } = require('discord.js');

const Redis = require('redis');
const Bluebird = require('bluebird');
const Cron = require('node-cron');

const { createLogger, format, transports } = require('winston');
const { combine, timestamp, printf } = format;

const myFormat = printf(({ level, message, timestamp }) => {
    return `${timestamp} [${level}] ${message}`;
});

const logger = createLogger({
    format: combine(
        timestamp(),
        myFormat
    ),
    transports: [new transports.Console()]
});

const Config = require('./config.json');
const SpelTip = require('./lib/speltips');
const Helpers = require('./lib/helpers');
const BlackJackService = require('./lib/blackjack');
const RouletteService = require('./lib/roulette');
const CreditsService = require('./lib/credits');
const PlayerFactory = require('./lib/player');

const discordClient = new Client({ intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES, Intents.FLAGS.GUILD_PRESENCES] });
let configuredChannel = null;

//Bluebird allows Async calls for redis
Bluebird.promisifyAll(Redis.RedisClient.prototype);
const redisClient = (process.env.REDIS_URL ? Redis.createClient(process.env.REDIS_URL) : Redis.createClient());

logger.info(`Connecting to redis : ${process.env.REDIS_URL}`);

const Credits = new CreditsService(redisClient, logger);
const Player = new PlayerFactory(Config, logger);
const BlackJack = new BlackJackService(Helpers, Credits, logger);
const Roulette = new RouletteService(Helpers, Credits, logger);

redisClient.on('error', function (err) {
    logger.error('Error ' + err);
});

// Schedule free credits
Cron.schedule('0 */9 * * *', function () {
    Player.allFromChannel(configuredChannel).forEach(function(player) {
        if (player.isOnline) {
            Credits.addCredits(player.creditsId, 5000)
        }
    });

    configuredChannel.send('In plaats van een Hostie :fortune_cookie: geeft de kikkerbot gratis credits aan iedereen die online is.');
});

function addCreditsToEveryone(message, amount) {
    Player.allFromChannel(message.channel).forEach(function(player) {
        Credits.addCredits(player.creditsId, amount)
    });
}

function setCreditsForEveryone(message, amount) {
    Player.allFromChannel(message.channel).forEach(function(player) {
        Credits.setCredits(player.creditsId, amount);
    });
}

discordClient.on('ready', () => {
    configuredChannel = discordClient.channels.cache.get(Config.channel_id);
    logger.info(`Bot has started, with ${discordClient.users.cache.size} users, in ${discordClient.channels.cache.size} channels of ${discordClient.guilds.cache.size} guilds.`);
    discordClient.user.setActivity(`Gambling with ${Player.allFromChannel(configuredChannel).length} players`);
});

discordClient.on('messageCreate', async message => {
    // Ignore bots
    if(message.author.bot) return;

    // Only filter messages with our prefix
    if(message.content.indexOf(Config.prefix) !== 0) return;

    const player = Player.fromMessage(message);
    if ( ! player) {
        return message.reply('Ah je bent nog geen echte kikker helaas.');
    }

    const args = message.content.slice(Config.prefix.length).trim().split(/ +/g);
    const command = args.shift().toLowerCase();

    if (player.isOwner) {
        if (command === 'debug') {
            //Debug / log stuff here
            return
        }

        if (command === 'save') {
            redisClient.internal_send_command({command: "BGSAVE", args: []});
            logger.info('Saving database');

            return
        }

        if (command === 'drop') {
            const freeCredits = Helpers.parseCredit(args[0]);
            if (freeCredits !== 0) {
                addCreditsToEveryone(message, freeCredits);
                player.sendMessage(`Iedereen heeft ${freeCredits} credits gekregen :confetti_ball:`);
            }

            return
        }

        if (command === 'reset-all') {
            setCreditsForEveryone(message, 0);

            return
        }

        if (BlackJack.isActive() && command === 'decks') {
            BlackJack.setDeckCount(player, Helpers.parseCredit(args[0]));

            return;
        }
    }

    if (command === 'help') {
        const helpMessage = 'Credits: !credits, !give @mention 100, !reset, !open (gift)\n' +
            'Roulette: !roulette, !bet 100 :red_circle:, :black_circle: or :green_circle:)\n' +
            'Blackjack: !blackjack !bet 100, !bet 200 continue, !hit, !stand, !double, !stop (continue betting), !count\n' +
            'Other: !top, !players, !speltip, !hi, !ping\n' +
            'Admin only: !drop, !reset-all, !save, !debug, !decks\n';
        player.sendMessage(helpMessage);

        return
    }

    if (command === 'roulette') {
        Roulette.startRoulette(player);

        return
    }

    if (command === 'blackjack') {
        BlackJack.start(player);

        return
    }

    if (BlackJack.isActive()) {
        if (command === 'hit') {
            BlackJack.hit(player);

            return
        }

        if (command === 'double') {
            BlackJack.doubleDown(player);

            return
        }

        if (command === 'stand') {
            BlackJack.stand(player);

            return
        }

        if (command === 'stop') {
            BlackJack.removeBetContinue(player);

            return
        }

        if (command === 'count') {
            player.sendMessage(`Current running count : ${BlackJack.getCardCount()}, decks remaining : ${BlackJack.getDecksRemaining()}`);

            return
        }
    }

    if (command === 'speltip') {
        if (args[0] === undefined) {
            player.sendMessage(SpelTip.parseTip(Helpers.randomSpelTipNumber()));
        } else {
            const tip = SpelTip.parseTip(args[0]);
            if (tip !== 'unknown') {
                player.sendMessage(tip);
            }
        }

        return
    }

    if (command === 'give' && args.length >= 2) {
        const recipient = Player.fromMention(message, args[0]);
        const amount = Helpers.parseCredit(args[1]);

        if (recipient && amount) {
            if (recipient.id === player.id) {
                logger.info(`Cannot add credits to yourself ${player.username}!`);
                return;
            }

            Credits.getCredits(player.creditsId).then(function (credits) {
                if (amount <= credits) {
                    Credits.removeCredits(player.creditsId, amount);
                    Credits.addCredits(recipient.creditsId, amount);

                    player.sendMessage(`${player.name} heeft ${amount} credits gegeven aan ${recipient.name} :heart_eyes:`);
                }
            })
        }

        return
    }

    if (command === 'credits') {
        Credits.getCredits(player.creditsId).then(function (credits) {
            player.sendMessage(`${player.name} heeft ${credits} credits.`);
        });

        return
    }

    if (command === 'players') {
        const players = Player.allFromChannel(message.channel);
        const playerNames = players.map(function(player) {
            return player.name;
        }).join(', ');

        player.sendMessage(`Currently ${players.length} players : ${playerNames}`);

        return
    }

    if (command === 'reset') {
        Credits.setCredits(player.creditsId, 0);
        player.sendMessage(`${player.name} heeft niks meer :( `);

        return
    }

    if (command === 'top') {
        const ranking = [];
        Player.allFromChannel(message.channel).forEach(function(player) {
            ranking.push(Credits.getCredits(player.creditsId).then(function (credits) {
                return {name: player.name, credits: credits}
            }))
        });

        Promise.all(ranking).then(function (list) {
            list.sort((a,b) => b.credits - a.credits);

            let top3Message = '';
            list.slice(0, 3).forEach(function (item, i) {
                if (i === 0) {
                    top3Message += ':first_place: ';
                } else if (i === 1) {
                    top3Message += ':second_place: ';
                } else if (i === 2) {
                    top3Message += ':third_place: ';
                }
                top3Message += `${item.name} : ${item.credits}\n`;
            });

            player.sendMessage(top3Message);
        });

        return
    }

    if (command === 'bet') {
        let allIn = false;
        let betAmount = 0;
        if (isNaN(args[0])) {
            if (args[0] === 'all') {
                allIn = true;
            } else {
                return
            }
        } else {
            betAmount = Helpers.parseCredit(args[0]);

            if (betAmount === 0) {
                return
            }
        }

        Credits.getCredits(player.creditsId).then(function (credits) {
            if (allIn) {
                betAmount = credits;
                logger.info(`${message.author.username} all in with ${betAmount}`);
            }

            if (credits === 0) {
                message.channel.send(`${message.author.username} je kan niet inzetten als je niks hebt.`);
                return
            }

            if (betAmount > credits) {
                message.channel.send(`${message.author.username} je hebt maar ${credits} credits.`);
                return
            }

            //Roulette bets
            if (Roulette.isActive() && args[1]) {
                Roulette.setRouletteBet(player, betAmount, args[1].codePointAt(0));
            } else if (BlackJack.isActive() && !BlackJack.isDrawing()) {
                if (args[1] === 'continue') {
                    BlackJack.setBetContinue(player, betAmount);
                } else {
                    BlackJack.setBet(player, betAmount);
                }
            }

            // TODO: make rolling dice betting with !dice
            // const dice = Helpers.randomBet();
            // var gambleMessage = '';
            // if (allIn || (betAmount === credits)) {
            //     gambleMessage = `:bell: ${message.author.username} gaat all in :bell: met ${credits} credits en gooit ${dice}.\n`;
            // } else {
            //     gambleMessage = `${message.author.username} zet ${betAmount} in en gooit ${dice}.\n`;
            // }
            //
            // if (dice === 1) {
            //     Credits.addCredits(player.creditsId, betAmount);
            //     player.sendMessage(gambleMessage + `${player.name} heeft ${betAmount} gewonnen en staat nu op ${credits + betAmount}.`);
            // } else {
            //     Credits.removeCredits(player.creditsId, betAmount);
            //     player.sendMessage(gambleMessage + `${player.name} is zijn inzet kwijt en staat op ${credits - betAmount}.`);
            // }
        });

        return
    }

    if (command === 'hi') {
        message.channel.send(`Heeeey ${message.author.username} een echte kikker!`);

        return;
    }

    if(command === 'ping') {
        // Calculates ping between sending a message and editing it, giving a nice round-trip latency.
        // The second ping is an average latency between the bot and the websocket server (one-way, not round-trip)
        const m = await message.channel.send('Ping?');
        m.edit(`Pong! Latency is ${m.createdTimestamp - message.createdTimestamp}ms. API Latency is ${Math.round(discordClient.ping)}ms`);
    }
});

discordClient.login(Config.token);

