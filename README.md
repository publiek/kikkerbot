### Kikkerbot met docker

configureer je `REDIS_URL` voor de redis server

rename `config-example.json` naar `config.json` en zet de juiste waardes

install dependencies lokaal `npm install`

bot starten lokaal `npm start`

volume extern maken zodat je niet perongeluk je database weggooit `docker volume create --name=redisdata`

in docker `docker-compose build && docker-compose up -d`


